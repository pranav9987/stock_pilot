import React from 'react';
import {Provider} from 'react-redux';
import store from '../store';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './Screens/Home';
import StockDetails from './Screens/StockDetails';
import SearchStock from './Screens/SearchStock';
// {persist redux setup}
import {PersistGate} from 'redux-persist/integration/react';
import {persistStore} from 'redux-persist';

const Stack = createStackNavigator();
export default function App() {
  const persistedStore = persistStore(store);
  return (
    <Provider store={store}>
      <PersistGate persistor={persistedStore} loading={null}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Home">
            <Stack.Screen
              name="Home"
              component={Home}
              options={{headerShown: false}}
            />

            <Stack.Screen
              name="StockDetails"
              component={StockDetails}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="SearchStock"
              component={SearchStock}
              options={{headerShown: false}}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}
