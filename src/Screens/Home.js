/* eslint-disable react-native/no-color-literals */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  FlatList,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  getStocks,
  deleteFromWatchList,
  getStockDetailById,
} from '../actions/Stocks';
import HeaderBar from '../components/HeaderBar';
import Loader from '../components/Loader';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/Ionicons';
import {getLatestStockNews} from '../actions/News';
import {setEnabled} from 'react-native/Libraries/Performance/Systrace';
const {width, height} = Dimensions.get('window');

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(state => state.loadingReducer.isLoading);
  const [editMode, SetEditMode] = useState(false);
  const [updateComp, setUpdateComp] = useState(false);

  const watchListStock = useSelector(
    state => state.stockReducer.watchListStock,
  );

  useEffect(async () => {
    await dispatch(getStocks());
    dispatch(getLatestStockNews());
  }, []);
  useEffect(async () => {
    setUpdateComp(!updateComp);
  }, [watchListStock]);
  return (
    <View style={styles.homeContainer}>
      <StatusBar hidden={true} />
      <HeaderBar navigation={navigation} home={true} />
      <View style={styles.bodyContainer}>
        <Loader loading={isLoading} />
        <View
          style={{
            // borderWidth: 1,
            // borderColor: 'black',
            height: height * 0.9,
            width: width * 0.9,
            marginTop: height * 0.05,
          }}>
          <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
            <Text style={{fontWeight: 'bold'}}>Watchlist</Text>
            {!editMode ? (
              <Icon name="pencil" size={20} onPress={() => SetEditMode(true)} />
            ) : (
              <MaterialIcons
                name="cancel"
                size={20}
                onPress={() => SetEditMode(false)}
              />
            )}
          </View>
          <View style={{marginTop: 10}} />
          <FlatList
            style={
              {
                // width: width * 0.88,
                // height: height * 0.9,
              }
            }
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            data={watchListStock}
            renderItem={({item, index}) => {
              let {
                symbol = '',
                percentChange,
                priceChange,
                currentPrice = 'NA',
              } = item;

              return (
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 5,
                  }}
                  onPress={async () => {
                    await dispatch(getStockDetailById(item.id));
                    navigation.navigate('StockDetails', {stalk: item});
                  }}>
                  <View
                    style={{
                      justifyContent: 'space-around',
                      flexDirection: 'row',
                      borderWidth: 1,
                      borderColor: 'grey',
                      alignItems: 'center',
                      minHeight: height * 0.08,
                      width: width * 0.8,
                    }}>
                    <Text>{symbol}</Text>
                    <Text>{percentChange}%</Text>
                    <Text>{priceChange}</Text>
                    <Text>{currentPrice}</Text>
                  </View>
                  {editMode && (
                    <AntDesign
                      onPress={() => {
                        dispatch(deleteFromWatchList(item));
                        setUpdateComp(!updateComp);
                      }}
                      name="delete"
                      size={20}
                    />
                  )}
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </View>
    </View>
  );
};
export default Home;

const styles = StyleSheet.create({
  homeContainer: {flex: 1},
  header: {flexDirection: 'row', justifyContent: 'space-between'},
  bodyContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
