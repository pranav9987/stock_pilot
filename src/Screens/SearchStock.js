/* eslint-disable react-native/no-color-literals */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  FlatList,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SearchedStalks from '../components/SearchedStalks';
const {width, height} = Dimensions.get('window');
import {
  getStockDetailById,
  deleteFromWatchList,
  addToWatchList,
} from '../actions/Stocks';
const SearchStock = ({navigation}) => {
  const dispatch = useDispatch();
  const [stockName, setStockName] = useState('');
  const [updateComp, setUpdateComp] = useState(false);

  const stockList = useSelector(state => state.stockReducer.stockList);
  const stock = useSelector(state => state.stockReducer.stock);
  const watchListStock = useSelector(
    state => state.stockReducer.watchListStock,
  );

  const [newStockList, setNewStockList] = useState([]);
  useEffect(async () => {
    await setNewStockList(stockList);
  }, []);

  const _onBookMarkPress = async props => {
    const {id, name, symbol} = props;
    await dispatch(getStockDetailById(id));
    const stockObject = {
      name,
      id,
      symbol,
      priceChange: stock.market_data
        ? stock.market_data.price_change_24h
          ? stock.market_data.price_change_24h
          : 'NA'
        : 'NA',
      percentChange: stock.market_data
        ? stock.market_data.price_change_percentage_24h
          ? stock.market_data.price_change_percentage_24h
          : 'NA'
        : 'NA',
      currentPrice: stock.market_data
        ? stock.market_data.current_price.inr
          ? stock.market_data.current_price.inr
          : 'NA'
        : 'NA',
      sequence_number: watchListStock.length + 1,
    };
    // checking if stock is already bookmarked
    if (watchListStock.some(stock => stock.id === id)) {
      await dispatch(deleteFromWatchList(stockObject));
    } else {
      await dispatch(addToWatchList(stockObject));
    }
    setUpdateComp(!updateComp);
  };

  return (
    <View style={styles.stockContainer}>
      <StatusBar hidden={true} />
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="caret-back-outline" size={30} />
        </TouchableOpacity>
        <View
          style={{
            borderWidth: 1,
            borderColor: 'black',
            flexDirection: 'row',
          }}>
          <TextInput
            style={styles.inputStyle}
            onChangeText={async e => {
              if (e) {
                let newerStockList = stockList.filter(stock => {
                  return stock.symbol === null
                    ? null
                    : stock.symbol.toLowerCase().indexOf(e.toLowerCase()) !==
                        -1;
                });
                setStockName(e), setNewStockList(newerStockList);
              } else {
                setStockName(e), setNewStockList([]);
              }
            }}
            autoFocus
            value={stockName}
            placeholder="Enter your search stock"
            placeholderTextColor="#777777"
            maxLength={60}
          />
          <Ionicons name="search" size={30} />
        </View>
      </View>
      <View
        style={{
          marginLeft: width * 0.07,
          marginTop: height * 0.01,
        }}>
        <Text>Top Crypto Assets</Text>

        <FlatList
          style={{
            height: height * 0.9,
          }}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          data={newStockList}
          renderItem={({item, index}) => {
            let isMarked = false;
            if (watchListStock.some(watchStock => watchStock.id === item.id)) {
              isMarked = true;
            }
            return (
              <SearchedStalks
                stalk={item}
                navigation={navigation}
                _onBookMarkPress={_onBookMarkPress}
                isMarked={isMarked}
              />
            );
          }}
        />
      </View>
    </View>
  );
};
export default SearchStock;

const styles = StyleSheet.create({
  stockContainer: {flex: 1, marginTop: 5},
  header: {flexDirection: 'row'},
  inputStyle: {
    width: width * 0.78,
    marginLeft: width * 0.02,
  },
});
