/* eslint-disable react-native/no-color-literals */
import React, {useEffect} from 'react';
import {StyleSheet, View, StatusBar} from 'react-native';
import {useDispatch} from 'react-redux';
import HeaderBar from '../components/HeaderBar';
import StackMarketDetails from '../components/StackMarketDetails';
import Graph from '../components/Graph';
import News from '../components/News';
import {getStockGraph} from '../actions/Stocks';
const StockDetails = ({navigation, route}) => {
  const dispatch = useDispatch();

  let {stalk = {}} = route.params;
  useEffect(async () => {
    await dispatch(getStockGraph(stalk.id, 1));
  }, []);
  return (
    <View style={styles.stockContainer}>
      <StatusBar hidden={true} />
      <HeaderBar navigation={navigation} />
      <StackMarketDetails />
      <Graph stalk={stalk} />
      <View style={{alignItems: 'center', marginTop: 20}}>
        <News />
      </View>
    </View>
  );
};
export default StockDetails;

const styles = StyleSheet.create({
  stockContainer: {flex: 1},
});
