import {LOADING_START, LOADING_STOP} from './Types';

export const startLoadingSpinner = () => {
  return {
    type: LOADING_START,
  };
};

export const stopLoadingSpinner = () => {
  return {
    type: LOADING_STOP,
  };
};
