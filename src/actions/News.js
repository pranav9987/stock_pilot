import {GET_STOCK_NEWS} from './Types.js';
import axios from 'react-native-axios';
const base_news_anchor =
  'https://newsapi.org/v2/everything?domains=wsj.com&apiKey=799599c39cb444f7b95f20800bd5561c';
export const getLatestStockNews = () => dispatch => {
  return axios
    .get(`${base_news_anchor}`)
    .then(res => {
      dispatch({
        type: GET_STOCK_NEWS,
        payload: res.data,
      });
    })
    .catch(err => {
      console.log(err);
    });
};
