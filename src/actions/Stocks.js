import {
  GET_ALL_STOCKS,
  ADD_STOCK_TO_WATCH_LIST,
  DELETE_STOCK_FROM_WATCH_LIST,
  GET_STOCK_DETAIL,
  GET_STOCK_GRAPH,
  UPDATE_BOOK_MARKED_STOCKS,
} from './Types.js';
import {startLoadingSpinner, stopLoadingSpinner} from './Loader';
import axios from 'react-native-axios';
const base_stock_anchor = 'https://api.coingecko.com/api';

export const getStocks = () => dispatch => {
  return axios
    .get(`${base_stock_anchor}/v3/coins/list?include_platform=false`)
    .then(async res => {
      dispatch({
        type: GET_ALL_STOCKS,
        payload: res.data,
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const getStockDetailById = id => dispatch => {
  dispatch(startLoadingSpinner());
  return axios
    .get(
      `${base_stock_anchor}/v3/coins/${id}?tickers=true&market_data=true&community_data=true&developer_data=false&sparkline=false`,
    )
    .then(res => {
      dispatch(stopLoadingSpinner());
      dispatch({
        type: GET_STOCK_DETAIL,
        payload: res.data,
      });
    })
    .catch(err => {
      dispatch(stopLoadingSpinner());
      console.log(err);
    });
};
export const getStockGraph = (id, days) => dispatch => {
  return axios
    .get(
      `${base_stock_anchor}/v3/coins/${id}/market_chart?vs_currency=inr&days=${days}`,
    )
    .then(res => {
      dispatch(stopLoadingSpinner());
      let graph = [];
      if (Object.keys(res.data).length !== 0) {
        for (let volumes of res.data.total_volumes) {
          graph.push(volumes[1]);
        }
      }
      dispatch({
        type: GET_STOCK_GRAPH,
        payload: graph,
      });
    })
    .catch(err => {
      dispatch(stopLoadingSpinner());
      console.log(err);
    });
};

export const addToWatchList = stock => {
  return {
    type: ADD_STOCK_TO_WATCH_LIST,
    payload: stock,
  };
};
export const deleteFromWatchList = stock => {
  return {
    type: DELETE_STOCK_FROM_WATCH_LIST,
    payload: stock,
  };
};
