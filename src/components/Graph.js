/* eslint-disable react-native/no-color-literals */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import Ionicons from 'react-native-vector-icons/Ionicons';
const {width, height} = Dimensions.get('window');
import {getStockGraph} from '../actions/Stocks';
import {LineChart} from 'react-native-chart-kit';

const Graph = ({stalk}) => {
  const dispatch = useDispatch();
  const graphPoints = useSelector(state => state.stockReducer.graphPoints);
  const [duration, setDuration] = useState([
    {name: '1D', value: 1, selected: true},
    {name: '1W', value: 7, selected: false},
    {name: '1M', value: 30, selected: false},
    {name: '1Y', value: 365, selected: false},
    {name: 'Max', value: 'max', selected: false},
  ]);
  const [updateState, setUpdateState] = useState(false);
  return (
    <View>
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        {graphPoints.length !== 0 && (
          <LineChart
            data={{
              // labels: ['January', 'February', 'March', 'April', 'May', 'June'],
              datasets: [
                {
                  data: graphPoints,
                },
              ],
            }}
            width={width * 0.9} // from react-native
            height={height * 0.3}
            // yAxisLabel="$"
            // yAxisSuffix="k"
            yAxisInterval={1} // optional, defaults to 1
            chartConfig={{
              backgroundColor: 'white',
              backgroundGradientFrom: 'white',
              backgroundGradientTo: 'white',
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => 'skyblue',
              labelColor: (opacity = 1) => 'blue',
              style: {
                borderRadius: 16,
              },
              propsForDots: {
                r: '2',
                strokeWidth: '1',
                stroke: 'skyblue',
              },
            }}
            bezier
            style={{
              borderRadius: 16,
            }}
          />
        )}
      </View>
      <View
        style={{
          justifyContent: 'space-around',
          flexDirection: 'row',
        }}>
        {duration.map((dur, index) => {
          let {name, value, selected} = dur;
          return (
            <TouchableOpacity
              key={index}
              onPress={() => {
                dispatch(getStockGraph(stalk.id, value));
                let newSetDuration = duration;
                newSetDuration.forEach(e => {
                  if (e.value === duration[index].value) {
                    e.selected = true;
                  } else {
                    e.selected = false;
                  }
                });
                setDuration(newSetDuration);
                setUpdateState(!updateState);
              }}
              style={{
                width: width * 0.15,
                height: height * 0.05,
                borderColor: 'grey',
                borderWidth: 1,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
                backgroundColor: selected ? 'skyblue' : 'white',
              }}>
              <Text style={{fontWeight: 'bold'}}>{name}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};
export default Graph;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: 'grey',
    alignItems: 'center',
    borderStyle: 'dashed',
    width: width * 0.89,
    borderRadius: 1,
    paddingHorizontal: 5,
    marginBottom: 3,
    minHeight: height * 0.06,
  },
});
