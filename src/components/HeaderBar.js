/* eslint-disable react-native/no-color-literals */
import React from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Keyboard,
  StatusBar,
  Dimensions,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
const {width, height} = Dimensions.get('window');

const HeaderBar = ({navigation, home}) => {
  return (
    <View style={styles.header}>
      {home ? (
        <TouchableOpacity onPress={() => alert('This is a first page')}>
          <Ionicons name="menu" size={30} />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="caret-back-outline" size={30} />
        </TouchableOpacity>
      )}

      <TouchableOpacity onPress={() => navigation.navigate('SearchStock')}>
        <Ionicons name="search" size={30} />
      </TouchableOpacity>
    </View>
  );
};
export default HeaderBar;

const styles = StyleSheet.create({
  header: {flexDirection: 'row', justifyContent: 'space-between'},
});
