/* eslint-disable react-native/no-color-literals */
import React from 'react';
import {StyleSheet, View, Modal, ActivityIndicator} from 'react-native';

const Loader = props => {
  const {loading} = props;

  return (
    <>
      {loading && (
        <Modal
          transparent={true}
          animationType={'none'}
          visible={loading}
          onRequestClose={() => {
            console.log('close modal');
          }}>
          <View style={styles.modalBackground}>
            <View style={styles.activityIndicatorWrapper}>
              <ActivityIndicator
                animating={true}
                color="#000000"
                size="large"
                style={styles.activityIndicator}
              />
            </View>
          </View>
        </Modal>
      )}
    </>
  );
};

export default Loader;

const styles = StyleSheet.create({
  activityIndicator: {
    alignItems: 'center',
    height: 80,
  },
  activityIndicatorWrapper: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    display: 'flex',
    height: 100,
    justifyContent: 'space-around',
    width: 100,
  },
  modalBackground: {
    alignItems: 'center',
    backgroundColor: '#00000040',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
});
