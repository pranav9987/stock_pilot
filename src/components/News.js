/* eslint-disable react-native/no-color-literals */
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Dimensions,
  Image,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import {useSelector} from 'react-redux';
const News = props => {
  const stockNews = useSelector(state => state.newsReducer.stockNews);

  let data = [{}, {}];
  return (
    <View
      style={{
        height: height * 0.49,
      }}>
      <Text style={{fontWeight: 'bold'}}>News</Text>
      <FlatList
        style={
          {
            // width: width * 0.88,
            // borderWidth: 1,
            // borderColor: 'black',
          }
        }
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        data={stockNews}
        keyExtractor={item => item.title}
        renderItem={({item, index}) => {
          let {title = '', content = '', urlToImage = ''} = item;
          return (
            <View
              key={index}
              style={{
                width: width * 0.9,
                minHeight: height * 0.15,
                borderWidth: 1,
                borderColor: 'skyblue',
                padding: 4,
                marginBottom: 9,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View style={{width: width * 0.5}}>
                <Text style={{fontWeight: 'bold'}}>{title}</Text>

                <Text>{content}</Text>
              </View>
              <Image
                source={{uri: urlToImage}}
                style={{
                  width: width * 0.35,
                  height: height * 0.15,
                  marginRight: 4,
                }}
              />
              <View style={{width: width * 0.1}} />
            </View>
          );
        }}
      />
    </View>
  );
};

export default News;

const styles = StyleSheet.create({});
