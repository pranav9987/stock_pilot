/* eslint-disable react-native/no-color-literals */
import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
const {width, height} = Dimensions.get('window');
import {getStockDetailById} from '../actions/Stocks';
import {useDispatch} from 'react-redux';

const SearchedStalks = ({stalk, navigation, _onBookMarkPress, isMarked}) => {
  const dispatch = useDispatch();
  let {id = '', symbol = ''} = stalk;

  // console.log(index);
  return (
    <View>
      <View style={styles.header}>
        <TouchableOpacity
          onPress={async () => {
            await dispatch(getStockDetailById(id));
            navigation.navigate('StockDetails', {stalk});
          }}
          style={{width: width * 0.4}}>
          <Text>{symbol}</Text>
        </TouchableOpacity>

        <Ionicons
          onPress={() => _onBookMarkPress(stalk)}
          name={isMarked ? 'md-star-sharp' : 'md-star-outline'}
          color={isMarked ? '#fcd12a' : 'grey'}
          size={30}
        />
      </View>
    </View>
  );
};
export default SearchedStalks;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: 'grey',
    alignItems: 'center',
    borderStyle: 'dashed',
    width: width * 0.89,
    borderRadius: 1,
    paddingHorizontal: 5,
    marginBottom: 3,
    minHeight: height * 0.06,
  },
});
