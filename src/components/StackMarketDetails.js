/* eslint-disable react-native/no-color-literals */
import React, {useState} from 'react';
import {StyleSheet, View, Text, Dimensions} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
const {width, height} = Dimensions.get('window');
import {useSelector, useDispatch} from 'react-redux';
import {addToWatchList, deleteFromWatchList} from '../actions/Stocks';
const StackMarketDetails = () => {
  const stock = useSelector(state => state.stockReducer.stock);
  const watchListStock = useSelector(
    state => state.stockReducer.watchListStock,
  );
  const dispatch = useDispatch();
  let {symbol = '', name = '', id} = stock;
  let isMarked = false;
  if (watchListStock.some(watchStock => watchStock.id === id)) {
    isMarked = true;
  }
  const [updateComp, setUpdateComp] = useState(false);

  const _handleMarking = async () => {
    const stockObject = {
      name,
      id,
      symbol,
      priceChange: stock.market_data
        ? stock.market_data.price_change_24h
          ? stock.market_data.price_change_24h
          : 'NA'
        : 'NA',
      percentChange: stock.market_data
        ? stock.market_data.price_change_percentage_24h
          ? stock.market_data.price_change_percentage_24h
          : 'NA'
        : 'NA',
      currentPrice: stock.market_data
        ? stock.market_data.current_price.inr
          ? stock.market_data.current_price.inr
          : 'NA'
        : 'NA',
      sequence_number: watchListStock.length + 1,
    };
    if (watchListStock.some(stock => stock.id === id)) {
      await dispatch(deleteFromWatchList(stockObject));
    } else {
      await dispatch(addToWatchList(stockObject));
    }
    setUpdateComp(!updateComp);
  };
  return (
    <View
      style={{
        flexDirection: 'row',
        paddingHorizontal: width * 0.02,
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: width * 0.2,
        }}>
        <Text style={{fontWeight: 'bold'}}>{name}</Text>
        <Text>{` ${symbol} `}</Text>
        <Ionicons
          onPress={_handleMarking}
          name={isMarked ? 'md-star-sharp' : 'md-star-outline'}
          color={isMarked ? '#fcd12a' : 'grey'}
          size={30}
        />
      </View>
      <View style={{alignItems: 'center',width: width * 0.4}}>
        <Text style={{fontWeight: 'bold'}}>
          ₹
          {stock.market_data
            ? stock.market_data.current_price.inr
              ? stock.market_data.current_price.inr
              : 'NA'
            : 'NA'}
        </Text>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          {/* <Ionicons name="chevron-up" size={20} /> */}
          <View style={{flexDirection: 'row'}}>
            <Text>
              {stock.market_data
                ? stock.market_data.price_change_24h
                  ? stock.market_data.price_change_24h
                  : 'NA'
                : 'NA'}
            </Text>
            <Text>
              {`(${
                stock.market_data
                  ? stock.market_data.price_change_percentage_24h
                    ? stock.market_data.price_change_percentage_24h
                    : 'NA'
                  : 'NA'
              }%)`}
            </Text>
          </View>

          <Text>today</Text>
        </View>
      </View>
    </View>
  );
};
export default StackMarketDetails;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: 'grey',
    alignItems: 'center',
    borderStyle: 'dashed',
    width: width * 0.89,
    borderRadius: 1,
    paddingHorizontal: 5,
    marginBottom: 3,
    minHeight: height * 0.06,
  },
});
