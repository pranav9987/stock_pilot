// import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';

// import index from './src/reducers/index';

// const initialState = {};

const middleware = [thunk];
// const composeEnhancers =
//   typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
//     ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
//     : compose;

// const enhancer = composeEnhancers(applyMiddleware(...middleware));

// const store = createStore(index, initialState, enhancer);

// export default store;
import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import {persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import index from './src/reducers/index';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['stockReducer'],
};

const persistedReducer = persistReducer(persistConfig, index);

const store = createStore(persistedReducer, applyMiddleware(...middleware));
export default store;
